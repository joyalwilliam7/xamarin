﻿using JesusVision.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace JesusVision.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MenuPage : ContentPage
    {
        MainPage RootPage { get => Application.Current.MainPage as MainPage; }
        List<HomeMenuItem> menuItems;
        public MenuPage()
        {
            InitializeComponent();

            menuItems = new List<HomeMenuItem>
            {
                new HomeMenuItem {Id = MenuItemType.Browse, Icon="ion-md-people", Title="CONNECT" },
                new HomeMenuItem {Id = MenuItemType.About, Icon="ion-md-calendar", Title="EVENTS" },
                new HomeMenuItem {Id = MenuItemType.Sermons, Icon="ion-md-microphone", Title="SERMONS" },
                new HomeMenuItem {Id = MenuItemType.About, Icon="ion-md-log-in", Title="CHECKIN" },
                new HomeMenuItem {Id = MenuItemType.Testimonials, Icon="ion-md-text", Title="VIEW TESTIMONIALS" },
                new HomeMenuItem {Id = MenuItemType.Announcements, Icon="ion-md-megaphone", Title="ANNOUNCEMENT" },
                new HomeMenuItem {Id = MenuItemType.About, Icon="ion-md-heart", Title="GIVE" },
                new HomeMenuItem {Id = MenuItemType.About, Icon="ion-md-globe", Title="WEBSITE" },
                new HomeMenuItem {Id = MenuItemType.About, Icon="ion-md-paper", Title="PROGRAM SHEETS" },
                new HomeMenuItem {Id = MenuItemType.About, Icon="ion-logo-facebook", Title="SOCIAL MEDIA" },
                new HomeMenuItem {Id = MenuItemType.Profile, Icon="ion-md-person", Title="PROFILE" },
                new HomeMenuItem {Id = MenuItemType.About, Icon="ion-md-power", Title="LOGOUT" }
            };

            ListViewMenu.ItemsSource = menuItems;

            ListViewMenu.SelectedItem = menuItems[0];
            ListViewMenu.SelectionChanged += async (sender, e) =>
            {
                if (e.CurrentSelection.FirstOrDefault() == null)
                    return;
                if (((HomeMenuItem)e.CurrentSelection.FirstOrDefault()).Title.Equals("LOGOUT")){

                    Application.Current.MainPage = new LoginPage();
                }
                else
                {
                    var id = (int)((HomeMenuItem)e.CurrentSelection.FirstOrDefault()).Id;
                    await RootPage.NavigateFromMenu(id);
                }
                
            };
        }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
           
            if (((HomeMenuItem)((TappedEventArgs)e).Parameter).Title.Equals("LOGOUT"))
            {

                Application.Current.MainPage = new LoginPage();
                
            }
            else
            {
                var id = (int)((HomeMenuItem)((TappedEventArgs)e).Parameter).Id;
                await RootPage.NavigateFromMenu(id);
            }
          
        }
    }
}