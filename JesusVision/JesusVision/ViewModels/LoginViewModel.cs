﻿using JesusVision.Models;
using JesusVision.Views;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace JesusVision.ViewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public string username;
        public string password;

        public LoginViewModel()
        {

        }
        public string Username {
            get { return username; }
            set
            {
                username = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Username"));
            }
        }
        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Password"));
            }
        }
        public Command LoginCommand
        {
            get
            {
                return new Command(Login);
            }
        }
        private async void Login()
        {
            //null or empty field validation, check weather email and password is null or empty  
            if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
                await App.Current.MainPage.DisplayAlert("Empty Values", "Please enter Email and Password", "OK");
            else
            {
                if (Username != null && Password != null)
                {
                    var client = new RestClient(Constants.API + Constants.API_Login);
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Content-Type", "application/json");
                    request.AddParameter("application/json", "{\"username\":\""+ Username + "\",\"password\":\""+password+"\"}", ParameterType.RequestBody);
                    var response = await client.ExecuteAsync<LoginRes>(request);
                    if(response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        Application.Current.Properties.Clear();
                        Application.Current.Properties.Add("token", response.Data.id);
                        Application.Current.Properties.Add("userId", response.Data.userId);
                        App.Current.MainPage = new MainPage();
                    }else if(response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        await App.Current.MainPage.DisplayAlert("Login Fail", "Please enter correct Username and Password", "OK");
                    }
                    else
                    {
                        await App.Current.MainPage.DisplayAlert("Login Fail", "Please enter correct Username and Password", "OK");
                    }

                }
                else
                   await App.Current.MainPage.DisplayAlert("Login Fail", "Please enter correct Username and Password", "OK");
            }
        }
    }
}
