﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using JesusVision.Models;
using JesusVision.Views;
using System.ComponentModel;
using System.Windows.Input;
using PanCardView.Extensions;
using RestSharp;

namespace JesusVision.ViewModels
{
    public class SermonsViewModel : BaseViewModel
    {
		public event PropertyChangedEventHandler PropertyChanged;

		private int _currentIndex;
		private int _imageCount = 1078;
		private string _chapter;
        public Verse _verse;

        public string verse_text { get; set; }
       
        public SermonsViewModel()
		{
			Items = new ObservableCollection<object>
			{
				new { Source = CreateSource(), Ind = _imageCount++, Color = Color.Red, Title = "First" },
				new { Source = CreateSource(), Ind = _imageCount++, Color = Color.Green, Title = "Second" },
				new { Source = CreateSource(), Ind = _imageCount++, Color = Color.Gold, Title = "Long Title" },
				new { Source = CreateSource(), Ind = _imageCount++, Color = Color.Silver, Title = "4" },
				new { Source = CreateSource(), Ind = _imageCount++, Color = Color.Blue, Title = "5th" }
			};

			PanPositionChangedCommand = new Command(v =>
			{
				if (IsAutoAnimationRunning || IsUserInteractionRunning)
				{
					return;
				}

				var index = CurrentIndex + (bool.Parse(v.ToString()) ? 1 : -1);
				if (index < 0 || index >= Items.Count)
				{
					return;
				}
				CurrentIndex = index;
			});

			RemoveCurrentItemCommand = new Command(() =>
			{
				if (Items.Count == 0)
				{
					return;
				}
				Items.RemoveAt(CurrentIndex.ToCyclicalIndex(Items.Count));
			});

			GoToLastCommand = new Command(() =>
			{
				CurrentIndex = Items.Count - 1;
			});

            GetDailyBiblbeWords();
        }

		public ICommand PanPositionChangedCommand { get; }

		public ICommand RemoveCurrentItemCommand { get; }

		public ICommand GoToLastCommand { get; }

		public int CurrentIndex
		{
			get => _currentIndex;
			set
			{
				_currentIndex = value;
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CurrentIndex)));
			}
		}

        public Verse Verse
        {
            get => _verse;
            set
            {
                if (_verse != value)
                {
                    _verse = value;
                    OnPropertyChanged("Verse");
                }
            }
        }
       
        public bool IsAutoAnimationRunning { get; set; }

		public bool IsUserInteractionRunning { get; set; }

		public ObservableCollection<object> Items { get; }

		private string CreateSource()
		{
			var source = $"https://picsum.photos/500/500?image={_imageCount}";
			return source;
		}

        private async void GetDailyBiblbeWords()
        {
            const string ApiPath = "https://beta.ourmanna.com/api/v1/get/?format=json";
            var client = new RestClient(ApiPath);
            var request = new RestRequest(Method.GET);
            try
            {
                var youversion = await client.ExecuteAsync<Youversion>(request);

                Verse = youversion.Data.verse;
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Error: {e.Message}");
            }
        }
    }
}