﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using JesusVision.Services;
using JesusVision.Views;

namespace JesusVision
{
    public partial class App : Application
    {

        public App()
        {
            Device.SetFlags(new[] { "Shapes_Experimental" });
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            if(Application.Current.Properties.Count > 0)
                MainPage = new MainPage();
            else
                MainPage = new LoginPage();
            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.IoniconsModule());
           

        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
