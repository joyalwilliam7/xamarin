﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JesusVision.Models
{
    public enum MenuItemType
    {
        Browse,
        About,
        Sermons,
        Profile,
        Testimonials,
        Announcements
    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }
        public string Icon { get; set; }
    }
}
