﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JesusVision.Models
{
    public class LoginRes
    {
        public string id { get; set; }
        public int ttl { get; set; }
        public DateTime created { get; set; }
        public string userId { get; set; }

    }

}
