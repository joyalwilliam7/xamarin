﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace JesusVision.Models
{
    public class Details
    {
        public string text { get; set; }
        public string reference { get; set; }
        public string version { get; set; }
        public string verseurl { get; set; }

    }

    public class Verse
    {
        public Details details { get; set; }
        public string notice { get; set; }

    }

   public class Youversion

    {
        public Verse verse { get; set; }

    }
}
